//
//  ThingsWithNames.swift
//  TestTypes
//
//  Created by Steven Hertz on 12/20/19.
//  Copyright © 2019 DevelopItSolutions. All rights reserved.
//

import Foundation

protocol ThingsWithNames {
    var name: String { get set }
}
