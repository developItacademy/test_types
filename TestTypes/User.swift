//
//  User.swift
//  TestTypes
//
//  Created by Steven Hertz on 12/20/19.
//  Copyright © 2019 DevelopItSolutions. All rights reserved.
//

import Foundation

struct User {
    var name: string
    var age : Int
}
