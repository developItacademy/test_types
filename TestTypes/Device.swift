//
//  Device.swift
//  TestTypes
//
//  Created by Steven Hertz on 12/20/19.
//  Copyright © 2019 DevelopItSolutions. All rights reserved.
//

import Foundation

struct Device {
    var name: string
    var charge : Int
}
